var express = require('express');
var multer  = require('multer')
var upload = multer({ dest: 'uploads/' })
var router = express.Router();

/* POST fileUpload */
router.post('/', upload.single('fileUpload'), function(req, res, next) {
  var result = {
      size : req.file.size
  };
  
  res.set('Content-Type', 'application/json');
  res.send(result);
});

module.exports = router;
